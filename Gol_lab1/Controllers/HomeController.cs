﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Gol_lab1.Models;

namespace Gol_lab1.Controllers
{
    public class HomeController : Controller
    {
        CreditContext db = new CreditContext();
        public ActionResult Index()
        {
            IEnumerable<CreditType> types = db.CreditTypes;
            ViewBag.CreditTypes = types;
            return View();
        }
    }
}