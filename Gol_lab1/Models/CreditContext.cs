﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Gol_lab1.Models
{
    public class CreditContext
    {
        public DbSet<CreditType> CreditTypes { get; set; }
        public DbSet<Credit> Credits { get; set; }
    }
}