﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gol_lab1.Models
{
    public class Credit
    {
        public int Id { get; set; }
        public int CreditTypeId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Age { get; set; }
        public string INN { get; set; }
        public string PassportNumber { get; set; }
    }
}