﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gol_lab1.Models
{
    public class CreditType
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Limit { get; set; }
        public int Percent { get; set; }
        public int Monthes { get; set; }
    }
}